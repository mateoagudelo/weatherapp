<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="200" alt="Laravel Logo"></a></p>
<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://cdn.icon-icons.com/icons2/2699/PNG/512/vuejs_logo_icon_169247.png" width="150" alt="Laravel Logo"></a></p>

## Install Project

**This project needs a database (mysql). Remember to configure the .env file in the root directory with your database accesses.**

**You must also include the Google Maps and OpenWeather API keys in the .env file.**

Execute the following commands after configuring the database:

1. composer update
1. npm install 
1. npm run dev
1. php artisan migrate:fresh --seed
1. php artisan serve

_You can now access http://localhost:8000 and view the project._

### [Demo](https://heuristic-pare.51-222-137-15.plesk.page/)
