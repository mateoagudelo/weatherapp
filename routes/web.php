<?php

use Illuminate\Support\Facades\{Route, Artisan, App};
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Install migrations
Route::get('/install', function (){
    if(App::environment('local')) {
        Artisan::call('migrate:fresh', [
            '--seed' => true,
        ]);
    }
});

//Return Vue App
Route::view('/{all}', 'index')->where('all', '.*');
