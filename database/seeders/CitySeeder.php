<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\City;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        City::insert([
            ['name' => 'Miami', 'latitude' => 25.77, 'longitude' => -80.19],
            ['name' => 'Orlando', 'latitude' => 28.54, 'longitude' => -81.38],
            ['name' => 'New York', 'latitude' => 40.71, 'longitude' => -74.01],
        ]);
    }
}
