<?php

namespace App\Repository;

interface RepositoryInterface
{
    public function all();
    public function create(array $data);
    public function find(int $id);
}
