<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class HistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'humidity' => $this->humidity,
            'created_at' => $this->created_at->diffForHumans(),
            'city' => $this->city->name,
            'latitude' => $this->city->latitude,
            'longitude' => $this->city->longitude
        ];
    }
}
