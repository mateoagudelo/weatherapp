<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\CityResource;
use App\Repository\CityRepository;
use Illuminate\Http\Request;

class CityController extends Controller
{
    protected $cityRepository;

    /**
     * We instantiate the repository of the cities.
     */
    public function __construct(CityRepository $cityRepository)
    {
        //
        $this->cityRepository = $cityRepository;
    }

    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        //
        return CityResource::collection($this->cityRepository->all());
    }
}
