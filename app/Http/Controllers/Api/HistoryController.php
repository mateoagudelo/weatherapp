<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\HistoryRequest;
use App\Http\Resources\Api\HistoryResource;
use App\Repository\HistoryRepository;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    protected $historyRepository;

    /**
     * We instantiate the repository of the histories.
     */
    public function __construct(HistoryRepository $historyRepository)
    {
        $this->historyRepository = $historyRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(HistoryRequest $request)
    {
        //
        return new HistoryResource($this->historyRepository->create($request->validated()));
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        //
        return new HistoryResource($this->historyRepository->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
