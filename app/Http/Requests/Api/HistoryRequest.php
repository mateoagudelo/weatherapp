<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class HistoryRequest extends FormRequest
{
    /**
     * Append client ip to request.
     */
    public function prepareForValidation()
    {
        $this->merge([
            'ip_address' => request()->ip()
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'ip_address' => 'string|nullable',
            'city_id' => 'required|numeric',
            'humidity' => 'required|numeric'
        ];
    }
}
