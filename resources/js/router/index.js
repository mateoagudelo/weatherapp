import {createRouter, createWebHistory } from "vue-router";
import weatherComponent from "@/components/weatherComponent.vue";
import historyComponent from "@/components/historyComponent.vue";

const routes = [
    {
        path: "/",
        name: 'home',
        component: weatherComponent,
    },
    {
        path: '/view-history/:id',
        name: 'view',
        component: historyComponent
    }
];

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes,
});

export default router;
