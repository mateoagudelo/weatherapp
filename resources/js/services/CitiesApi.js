export default {
     getCities() {
        return axios.get('/api/city/')
            .then((response) => {
                return response.data.data
            })
    }
}
