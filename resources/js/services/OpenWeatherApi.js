export default {

    getData(latitude, longitude) {
        return fetch(`https://api.openweathermap.org/data/3.0/onecall?lat=${latitude}&lon=${longitude}&appid=${import.meta.env.VITE_OPENWEATHER_API_KEY}`)
            .then(response => response.json())
            .then((obj) => {
                return obj
            })
    }

}
