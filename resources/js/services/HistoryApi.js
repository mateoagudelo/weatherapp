export default {

    create(id, humidity) {
        return axios.post('/api/history', {city_id: id, humidity: humidity})
            .then((response) => {
                return response.data.data
            })
    },

    find(id) {
        return axios.get('/api/history/'+id)
            .then((response) => {
                return response.data.data
            })
    }

}
