import './bootstrap';

import axios from "axios";

import { createApp } from "vue";

import App from './App.vue';

import router from "./router/index";

const app = createApp(App)
app.config.globalProperties.$axios = axios
app.use(router)
app.mount('#app')
